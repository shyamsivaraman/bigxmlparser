package com.ar.xml.bigxml;

import org.ss.apps.bigxml.reader.IDGeneratingXMLReader;
import org.ss.apps.bigxml.reader.stax.StAXReader;
import org.ss.apps.bigxml.transform.XMLTransformationDriver;

import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by Shyam Sivaraman on 04-07-2016.
 */

public class Main {

    public static void main(String args[]) throws Exception {
        Main aMain = new Main();
        //aMain.parseXML("C:\\Users\\x\\IdeaProjects\\BigXML\\files\\XML_Small.xml");
        //aMain.runDriver("C:\\Users\\x\\IdeaProjects\\BigXML\\files\\XML_500M.xml");
        aMain.runDriver("C:\\Users\\x\\IdeaProjects\\BigXML\\files\\XML_1G.xml");
    }

    public void runDriver(String thePath) throws FileNotFoundException, XMLStreamException {
        StAXReader aStaxReader = new StAXReader(thePath);
        IDGeneratingXMLReader aReader = new IDGeneratingXMLReader(aStaxReader);

        XMLTransformationDriver xt = new XMLTransformationDriver(aReader);
        xt.init();
        xt.start();

        aReader.closeReader();
    }

    // 1 Million BOM parsed in 4 Mins. 15 Seconds
    // 1 Million BOM (16 tags per BOM Component) = 17 Million tags approx.

    public void parseXML(String thePath) throws IOException, XMLStreamException {
        StAXReader aStaxReader = new StAXReader(thePath);
        IDGeneratingXMLReader aReader = new IDGeneratingXMLReader(aStaxReader);

        long theStartMillis = System.currentTimeMillis();
        int i=0;
        while(aReader.hasNext()) {
            BigDecimal b = aReader.nextNode().getID();
            System.out.println(b);
        }
        System.out.println(">>>> Time to parse: " + (System.currentTimeMillis() - theStartMillis));

        aReader.closeReader();
    }
}
