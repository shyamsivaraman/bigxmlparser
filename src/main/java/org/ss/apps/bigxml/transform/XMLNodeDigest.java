package org.ss.apps.bigxml.transform;

import org.ss.apps.bigxml.XMLNode_I;

/**
 * Created by m2479 on 06-07-2016.
 */

public class XMLNodeDigest {

    private XMLNode_I m_XMLNode;
    private int m_TagHash = -1;
    private int m_TextHash = -1;

    public XMLNodeDigest(XMLNode_I theXMLNode) {
        this.m_XMLNode = theXMLNode;
    }

    public int getTagHash() {
        return this.m_TagHash;
    }

    public void setTagHash(int theHashCode) {
        this.m_TagHash = theHashCode;
    }

    public int getTextHash() {
        return this.m_TextHash;
    }

    public void setTextHash(int theHashCode) {
        this.m_TextHash = theHashCode;
    }

    public XMLNode_I getCoreNode() {
        return m_XMLNode;
    }

    public String toString() {
        return "TagHash: " + m_TagHash + ", TextHash: " + m_TextHash;
    }
}
