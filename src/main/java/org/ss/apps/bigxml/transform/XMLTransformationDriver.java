package org.ss.apps.bigxml.transform;

import org.ss.apps.bigxml.XMLNode_I;
import org.ss.apps.bigxml.XMLReader_I;
import org.ss.apps.bigxml.cache.CacheStrategy;
import org.ss.apps.bigxml.cache.StringTableCache;
import org.ss.apps.bigxml.cache.XMLNodeCache;

import javax.xml.stream.XMLStreamException;
import java.util.logging.Logger;

/**
 * Responsibilities for this class:
 * - Read the XML file node by node, assign IDs to each element
 * - Pass the XML node to the digester, and pass it to the cache for storage
 * - Pass the XML node to the StringTableCache for optimized storage and for fast indexed access
 *
 * @author Shyam Sivaraman on 06-07-2016.
 *
 */
public class XMLTransformationDriver {

    private XMLReader_I m_Reader;
    private static Logger m_Logger = Logger.getLogger("XMLTransformationDriver");

    public XMLTransformationDriver(XMLReader_I aReader) {
        this.m_Reader = aReader;
    }

    public void init() {
        m_Logger.info("XML transformation driver initialized");
    }

    public void start() throws XMLStreamException {
        if(m_Reader == null || !m_Reader.hasNext())
            return;

        XMLNodeDigester theDigester = new XMLNodeDigester(XMLNodeDigester.DIGEST_TYPE.OBJECT_HASH);

        XMLNodeCache theNodeCache = new XMLNodeCache(new CacheStrategy());  //TODO: Specify the strategy
        StringTableCache theSTableCache = new StringTableCache(new CacheStrategy());

        long theStartMillis = System.currentTimeMillis();

        while(m_Reader.hasNext()) {
            XMLNode_I theNode = m_Reader.nextNode();
            //m_Logger.info("Node: " + theNode.toString() + ", ID: " + theNode.getID());

            XMLNodeDigest theDigestedNode = theDigester.reduce(theNode);
            //m_Logger.info("Digested Node: " + theDigestedNode.toString());

            theNodeCache.put(theDigestedNode);
            theSTableCache.put(theDigestedNode);
        }

        m_Logger.info("Time to load the XML: " + (System.currentTimeMillis() - theStartMillis));
    }
}
