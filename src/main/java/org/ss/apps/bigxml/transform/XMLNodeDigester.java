package org.ss.apps.bigxml.transform;

import org.ss.apps.bigxml.XMLNode_I;

/**
 * Created by m2479 on 06-07-2016.
 * Takes in an XMLNode_I object and converts it to DigestedXMLNode and vice-versa
 */

public class XMLNodeDigester {

    public static enum DIGEST_TYPE { OBJECT_HASH, MD5_HASH, FNV1_HASH };

    private DIGEST_TYPE m_DigestType;

    public XMLNodeDigester(DIGEST_TYPE theDigestType) {
        this.m_DigestType = theDigestType;
    }

    public XMLNodeDigest reduce(XMLNode_I theXMLNode) {
        if(m_DigestType == DIGEST_TYPE.OBJECT_HASH) {
            return doObjectHash(theXMLNode);
        } else if(m_DigestType == DIGEST_TYPE.FNV1_HASH) {
            return doFnv1Hash(theXMLNode);
        } else {
            return doObjectHash(theXMLNode);
        }
    }

    private XMLNodeDigest doFnv1Hash(XMLNode_I theXMLNode) {
        //TODO: Implement FNV-1 hash with less collision rates
        return null;
    }

    private XMLNodeDigest doObjectHash(XMLNode_I theXMLNode) {
        XMLNodeDigest theXD = new XMLNodeDigest(theXMLNode);
        theXD.setTagHash(theXMLNode.getNodeName().hashCode());
        if(theXMLNode.getNodeType() == XMLNode_I.NodeType.NODE_TYPE_DATA && theXMLNode.getNodeValue() != null) {
            theXD.setTextHash(theXMLNode.getNodeValue().hashCode());
        }

        return theXD;
    }
}
