package org.ss.apps.bigxml.cache;

import org.ss.apps.bigxml.KeyValuePair;
import org.ss.apps.bigxml.XMLNode_I;
import org.ss.apps.bigxml.buffers.BufferPageSet;
import org.ss.apps.bigxml.buffers.XMLStringTableBuffer;
import org.ss.apps.bigxml.transform.XMLNodeDigest;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by m2479 on 06-07-2016.
 */

public class StringTableCache extends NodeCache_A<XMLNodeDigest> {

    private static Logger m_Logger = Logger.getLogger("StringTableCache");
    private static final int BUFFER_INIT_SIZE = 50;
    private static final int BUFFER_MAX_SIZE = 1024;
    private static final int BUFFER_INC_SIZE = BUFFER_INIT_SIZE;

    private Map<Integer, BufferPageSet> m_TagCacheStore = new LinkedHashMap<Integer, BufferPageSet>();
    private Map<Integer, BufferPageSet> m_TagTextStore = new LinkedHashMap<Integer, BufferPageSet>();

    public StringTableCache(CacheStrategy theCacheStrategy) {
        super(theCacheStrategy);
    }

    @Override
    public boolean put(XMLNodeDigest theNode) {
        //TODO: Put based on the cache strategy defined
        int thePrefix = theNode.getCoreNode().getLevel();

        BufferPageSet[] theBSets = getBufferSets(thePrefix);

        int theStringHash = theNode.getTagHash();
        String theTagText = theNode.getCoreNode().getNodeName();

        XMLStringTableBuffer theTagBuffer = getBufferForAppend(this.m_TagCacheStore, theBSets[0], thePrefix);
        KeyValuePair theInputValues = new KeyValuePair(theStringHash, theTagText);
        boolean isTagAppended = theTagBuffer.append(theInputValues);

        if(!isTagAppended) {
            theTagBuffer = createBuffer(thePrefix, theBSets[0].getActualSetSize());
            theBSets[0].add(theTagBuffer);
            theBSets[0].setActualSetSize(theBSets[0].getActualSetSize()+1);
            isTagAppended = theTagBuffer.append(theInputValues);
        }

        boolean isTextAppended = false;

        if(theNode.getCoreNode().getNodeType() == XMLNode_I.NodeType.NODE_TYPE_DATA) {
            int theTextHash = theNode.getTextHash();
            String theText = theNode.getCoreNode().getNodeValue();

            XMLStringTableBuffer theTextBuffer = getBufferForAppend(this.m_TagTextStore, theBSets[1], thePrefix);
            theInputValues = new KeyValuePair(theTextHash, theText);
            isTextAppended = theTextBuffer.append(theInputValues);

            if(!isTextAppended) {
                theTextBuffer = createBuffer(thePrefix, theBSets[1].getActualSetSize());
                theBSets[1].add(theTagBuffer);
                theBSets[1].setActualSetSize(theBSets[1].getActualSetSize()+1);
                isTextAppended = theTextBuffer.append(theInputValues);
            }
        }

        return isTagAppended || isTextAppended;
    }

    private XMLStringTableBuffer getBufferForAppend(Map<Integer, BufferPageSet> theCache, BufferPageSet theBufferSet, int thePrefix) {
        XMLStringTableBuffer aTagBuffer = null;

        if(theBufferSet == null) {
            theBufferSet = new BufferPageSet(thePrefix);
            aTagBuffer = createBuffer(thePrefix, 0);

            theBufferSet.add(aTagBuffer);
            theBufferSet.setActualSetSize(1);

            theCache.put(thePrefix, theBufferSet);
            m_CacheMiss++;
        } else {
            aTagBuffer = (XMLStringTableBuffer) getTailBuffer(theCache.get(thePrefix), thePrefix);
        }

        return aTagBuffer;
    }

    public BufferPageSet[] getBufferSets(int thePrefix) {
        return new BufferPageSet[] {m_TagCacheStore.get(thePrefix), m_TagTextStore.get(thePrefix)};
    }

    private XMLStringTableBuffer createBuffer(int thePrefix, int theContinuationIndex) {
        XMLStringTableBuffer theBuffer = new XMLStringTableBuffer(BUFFER_INIT_SIZE, BUFFER_MAX_SIZE, BUFFER_INC_SIZE);
        theBuffer.setPrefix(thePrefix);
        theBuffer.setContinuationIndex(theContinuationIndex);

        return theBuffer;
    }

    @Override
    public XMLNodeDigest get(int index) {
        return null;
    }

}
