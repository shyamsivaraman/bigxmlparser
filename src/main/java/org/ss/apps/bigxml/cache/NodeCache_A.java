package org.ss.apps.bigxml.cache;

import org.ss.apps.bigxml.buffers.BufferPageSet;
import org.ss.apps.bigxml.buffers.Buffer_I;

/**
 * Created by m2479 on 09-07-2016.
 */

public abstract class NodeCache_A<T> implements NodeCache_I<T> {

    protected CacheStrategy m_CachingStrategy;

    protected int m_CacheHit;
    protected int m_CacheMiss;

    protected NodeCache_A(CacheStrategy theCacheStrategy) {
        this.m_CachingStrategy = theCacheStrategy;
    }

    protected Buffer_I getTailBuffer(BufferPageSet theBSet, int thePrefix) {
        if(theBSet != null) {
            Buffer_I theBuffer = theBSet.get(theBSet.getActualSetSize()-1);
            if(theBuffer == null) {
                //TODO: If still null, then load it
                m_CacheMiss++;
                return null;
            }

            m_CacheHit++;
            return  theBuffer;
        }

        m_CacheMiss++;
        return null;
    }

    @Override
    public void setCacheStrategy(CacheStrategy theStrategy) {
        this.m_CachingStrategy = theStrategy;
    }

    @Override
    public int getCacheMisses() {
        return m_CacheMiss;
    }

    @Override
    public int getCacheHits() {
        return m_CacheHit;
    }

    public void reset() {
    }
}
