package org.ss.apps.bigxml.cache;

/**
 * Manages the caching of buffer pages using a caching scheme specified by the CacheStrategy. CacheStrategy is set by
 * the Driver. Buffer cache request input is a unique element ID of type BigDecimal. Various request types are:
 * - Get root: Element ID = 0.1
 * - Load children for Element ID (0.1) -> Load child page for (0.1 * 10) = 1
 * - Store a Digested Element
 * Created by Shyam Sivaraman on 06-07-2016.
 *
 */

public interface NodeCache_I<T> {

    void setCacheStrategy(CacheStrategy theStrategy);

    boolean put(T theNode);

    T get(int index);

    int getCacheMisses();

    int getCacheHits();

    void reset();

}
