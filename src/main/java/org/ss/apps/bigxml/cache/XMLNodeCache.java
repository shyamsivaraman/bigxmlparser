package org.ss.apps.bigxml.cache;

import org.ss.apps.bigxml.buffers.BufferPageSet;
import org.ss.apps.bigxml.buffers.XMLSiblingNodesBuffer;
import org.ss.apps.bigxml.transform.XMLNodeDigest;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * Created by m2479 on 06-07-2016.
 */

public class XMLNodeCache extends NodeCache_A<XMLNodeDigest> {

    private static Logger m_Logger = Logger.getLogger("XMLNodeCache");
    private static final int BUFFER_INIT_SIZE = 50;
    private static final int BUFFER_MAX_SIZE = 1024;
    private static final int BUFFER_INC_SIZE = BUFFER_INIT_SIZE;

    private Map<Integer, BufferPageSet> m_CacheStore = new LinkedHashMap<Integer, BufferPageSet>();

    public XMLNodeCache(CacheStrategy theCacheStrategy) {
        super(theCacheStrategy);
    }

    @Override
    public boolean put(XMLNodeDigest theNode) {
        //TODO: Put based on the cache strategy defined
        BigDecimal theID = theNode.getCoreNode().getID();
        int thePrefix = theID.intValue();
        //m_Logger.info("Node prefix determined: " + thePrefix);

        BufferPageSet theBSet = getBufferSet(thePrefix);
        XMLSiblingNodesBuffer aBuffer;

        if(theBSet == null) {
            theBSet = new BufferPageSet(thePrefix);
            aBuffer = createBuffer(thePrefix, 0);

            theBSet.add(aBuffer);
            theBSet.setActualSetSize(1);

            this.m_CacheStore.put(thePrefix, theBSet);
            m_CacheMiss++;
        } else {
            aBuffer = (XMLSiblingNodesBuffer)getTailBuffer(this.m_CacheStore.get(thePrefix), thePrefix);
        }

        Integer[] theInputValues = new Integer[]{theNode.getTagHash(), theNode.getTextHash()};
        boolean isAppended = aBuffer.append(theInputValues);

        if(!isAppended) {
            aBuffer = createBuffer(thePrefix, theBSet.getActualSetSize());
            theBSet.add(aBuffer);
            theBSet.setActualSetSize(theBSet.getActualSetSize()+1);
            isAppended = aBuffer.append(theInputValues);
        }

        return isAppended;
    }

    @Override
    public XMLNodeDigest get(int index) {
        return null;
    }

    private BufferPageSet getBufferSet(int thePrefix) {
        return m_CacheStore.get(thePrefix);
    }

    private XMLSiblingNodesBuffer createBuffer(int thePrefix, int theContinuationIndex) {
        XMLSiblingNodesBuffer theBuffer = new XMLSiblingNodesBuffer(BUFFER_INIT_SIZE, BUFFER_MAX_SIZE, BUFFER_INC_SIZE);
        theBuffer.setPrefix(thePrefix);
        theBuffer.setContinuationIndex(theContinuationIndex);

        return theBuffer;
    }
}
