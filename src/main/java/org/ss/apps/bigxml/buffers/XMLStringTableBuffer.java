package org.ss.apps.bigxml.buffers;

import org.ss.apps.bigxml.KeyValuePair;

import java.io.Serializable;
import java.util.logging.Logger;

/**
 * A String table buffer stores the strings found in a XML file. The tag strings and the text node strings
 * are stored in separate structures to facilitate fast search. The strings as well as their hash codes are
 * stored. Size of the buffer is restricted by the page size defined at instantiation time.
 *
 * The data stored by this class in normal mode:
 * - A sorted integer array of tag hashes and the string table. Array sort happens at the trim event, or at
 *   the end of merge event. Tag array is a single structure for a given tree level.
 * - A sorted integer array of text node hashes and its string table.
 * - Continuation buffer: A single buffer holds the data for a given level. The tag data may be less, while
 *   the text node data may span multiple buffer pages.
 * - The tree level for which this buffer is storing the data.
 * - Is the buffer dirty due to new tag additions/ deletions etc., if so the index locations.
 *
 * @author Shyam Sivaraman (shyamsivaraman@amberroad.com) on 07-07-2016.
 *
 */

public class XMLStringTableBuffer extends ArrayBuffer_A<KeyValuePair> implements Serializable {

    private static XMLStringTableBuffer m_PreparedInstance;

    private int[] m_HashBuffer;
    private String[] m_TextBuffer;

    private transient int m_BufferPointer = 0;
    private int m_Prefix;

    private static Logger m_Logger = Logger.getLogger("XMLStringTableBuffer");

    public XMLStringTableBuffer(int theStartBufferSize, int theMaxBufferSize, int theIncrementSize) {
        super(theMaxBufferSize, theIncrementSize);
        m_HashBuffer = new int[theStartBufferSize];
        m_TextBuffer = new String[theStartBufferSize];
    }

    @Override
    public boolean append(KeyValuePair theBufferElement) {
        if(m_BufferPointer == m_HashBuffer.length-1) {
            return false;
        }

        if(m_BufferPointer > (m_HashBuffer.length * m_LoadFactor * 0.01f)) {
            m_HashBuffer = growArray(m_HashBuffer);
            m_TextBuffer = growArray(m_TextBuffer);
        }

        m_HashBuffer[m_BufferPointer] = theBufferElement.m_Key;
        m_TextBuffer[m_BufferPointer] = theBufferElement.m_Value;
        m_BufferPointer++;

        return true;
    }

    @Override
    public boolean append(KeyValuePair[] theBufferChunk) {
        return false;
    }

    @Override
    public Buffer_I<KeyValuePair> merge(Buffer_I<KeyValuePair> theBufferToMerge) {
        return null;
    }

    @Override
    public void pack() {
    }

    @Override
    public void setPrefix(int thePrefix) {
        this.m_Prefix = thePrefix;
    }

    @Override
    public int getActualBufferSize() {
        return this.m_HashBuffer.length;
    }

    @Override
    public int[] getBufferPrefixes() {
        return new int[] {this.m_Prefix};
    }

    @Override
    public KeyValuePair get(int theIndex) {
        return null;
    }

        /****** Load methods ********/
    protected void deserialize() {

    }

    protected void serialize() {

    }
}
