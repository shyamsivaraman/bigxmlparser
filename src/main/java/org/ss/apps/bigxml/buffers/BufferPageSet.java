package org.ss.apps.bigxml.buffers;

import java.util.LinkedList;

/**
 * Created by m2479 on 07-07-2016.
 */

public class BufferPageSet {
    private int m_BufferPrefix;
    private int m_CachingLimit = -1;
    private int m_ActualSetSize = 0;
    private LinkedList<Buffer_I> m_Set = new LinkedList<Buffer_I>();

    public BufferPageSet(int thePrefix) {
        this.m_BufferPrefix = thePrefix;
    }

    public void add(Buffer_I aBuffer) {
        m_Set.addFirst(aBuffer);

        if(m_CachingLimit!=-1 && m_Set.size() > this.m_CachingLimit) {
            m_Set.removeLast();
        }
    }

    public Buffer_I get(int theSubIndex) {
        for(int i=0; i<m_Set.size(); i++) {
            if(m_Set.get(i).getContinuationIndex() == theSubIndex) {
                return m_Set.get(i);
            }
        }

        return null;
    }

    public void setCachingLimit(int theMaxSize) {
        this.m_CachingLimit = theMaxSize;
    }

    public void setBufferPrefix(int thePrefix) {
        this.m_BufferPrefix = thePrefix;
    }

    public int getBufferPrefix() {
        return m_BufferPrefix;
    }

    /**
     * When creating the buffers the actual size may show the total number of buffers created under this
     * set till now. When a buffer set is loaded, this values shows the actual number of buffer sets available
     * under this prefix, out of which some of them may not be loaded here due to caching limit restrictions.
     *
     * @param theActualSetSize
     */

    public void setActualSetSize(int theActualSetSize) {
        this.m_ActualSetSize = theActualSetSize;
    }

    public int getActualSetSize() {
        return this.m_ActualSetSize;
    }
}
