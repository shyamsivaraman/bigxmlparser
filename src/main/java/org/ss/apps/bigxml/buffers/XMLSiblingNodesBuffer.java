package org.ss.apps.bigxml.buffers;

import java.io.Serializable;
import java.util.logging.Logger;

/**
 * The sibling node buffer stores all the siblings for whom the parent is the same. The internal storage for
 * the buffer is an integer array storing the hash code of the tag names. The sequence of array matches the
 * sequence of the elements in the XML document. There are no missing sequences in the buffer. Insertions or
 * deletions to the buffer are dealt by another buffer.
 *
 * The data stored by this class when in normal mode:
 * - An integer array initialized to pre-defined size, with a max-defined size, and an actual size after a
 *   trim is done.
 * - An array of integers storing the hash codes for the text nodes.
 * - An integer representation of the bit array indicating a text node
 * - The common prefix (digit) part of all the sibling nodes (Persisting logic may use this as file name to
 *   store this data buffer
 * - Has continuation buffer: Pointer to the next buffer in case this buffer has a continuation buffer
 * - Is the buffer dirty due to new tag additions/ deletions etc., if so the index locations.
 *
 * Additional data stored by this class when in defragged mode:
 * - Array of prefixes which are merged into this file, and their start location in the buffer as 2D array
 *
 * The buffer is serializable as a page onto the persistent storage.
 *
 * @author Shyam Sivaraman (shyamsivaraman@amberroad.com) on 05-07-2016.
 *
 */

public class XMLSiblingNodesBuffer extends ArrayBuffer_A<Integer[]> implements Serializable {

    private int[] m_TagBuffer;
    private int[] m_TextBuffer;

    private transient int m_TagBufferPointer = 0;
    private transient int m_TextBufferPointer = 0;

    private int[][] m_BufferPrefixAndIndex = new int[1][1];
    private byte[] m_TextNodeBitArray;

    private static Logger m_Logger = Logger.getLogger("XMLSiblingNodesBuffer");

    public XMLSiblingNodesBuffer(int theStartBufferSize, int theMaxBufferSize, int theIncrementAmount) {
        super(theMaxBufferSize, theIncrementAmount);

        m_IncrementAmount = (theIncrementAmount - (theIncrementAmount%8)) + 8;

        theStartBufferSize = (theStartBufferSize - (theStartBufferSize%8)) + 8;
        m_TagBuffer = new int[theStartBufferSize];
        m_TextBuffer = new int[theStartBufferSize];

        theMaxBufferSize = (theMaxBufferSize - (theMaxBufferSize%8)) + 8;
        m_MaxBufferSize = theMaxBufferSize;
        m_TextNodeBitArray = new byte[theMaxBufferSize/8];
    }

    @Override
    public boolean append(Integer[] theBufferElement) {
        if(m_TagBufferPointer == m_TagBuffer.length-1) {
            return false;
        }

        if(m_TagBufferPointer > (m_TagBuffer.length * m_LoadFactor * 0.01f)) {
            m_TagBuffer = growArray(m_TagBuffer);
        }

        m_TagBuffer[m_TagBufferPointer++] = theBufferElement[0];

        if(theBufferElement.length > 1) {
            if (m_TextBufferPointer > (m_TextBufferPointer * m_LoadFactor * 0.01f)) {
                m_TextBuffer = growArray(m_TextBuffer);
            }

            //TODO: Shift 1 into the bit array
            m_TextBuffer[m_TextBufferPointer++] = theBufferElement[1];
        }
        //TODO: Shift 0 into the bit array

        return true;
    }

    public boolean append(Integer[][] theBufferChunk) {
        //TODO: Implement this method
        return false;
    }

    @Override
    public Buffer_I<Integer[]> merge(Buffer_I<Integer[]> theBufferToMerge) {
        return null;
    }

    private void trim() {
        //Trim to multiple of 8
    }

    @Override
    public void pack() {
        trim();
    }

    @Override
    public int getActualBufferSize() {
        return this.m_TagBuffer.length;
    }

    @Override
    public int[] getBufferPrefixes() {
        int[] thePrefixes = new int[m_BufferPrefixAndIndex.length];

        for(int i=0; i<m_BufferPrefixAndIndex.length; i++) {
            thePrefixes[i] = m_BufferPrefixAndIndex[i][0];
        }

        return thePrefixes;
    }

    /**
     * Only one prefix can be set externally on the buffer creation time.
     * Multiple prefix arrays are generated internally during a merge activity.
     * @param thePrefix
     */

    @Override
    public void setPrefix(int thePrefix) {
        this.m_BufferPrefixAndIndex[0] = new int[]{thePrefix, 0};
    }

    @Override
    public Integer[] get(int theIndex) {
        return null;
    }

}
