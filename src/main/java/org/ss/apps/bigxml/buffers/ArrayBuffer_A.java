package org.ss.apps.bigxml.buffers;

/**
 * Created by m2479 on 09-07-2016.
 */

public abstract class ArrayBuffer_A<T> implements Buffer_I<T> {

    protected transient short m_LoadFactor = 75;
    protected transient int m_MaxBufferSize;
    protected transient int m_IncrementAmount;

    private int m_ContinuationIndex  = 0;

    protected boolean m_IsTrimmed;
    private boolean m_IsDirty;

    public ArrayBuffer_A(int theMaxBufferSize, int theIncrementAmount) {
        this.m_MaxBufferSize = theMaxBufferSize;
        this.m_IncrementAmount = theIncrementAmount;
    }

    protected int[] growArray(int[] theArray) {
        int[] theNewArray = new int[theArray.length + m_IncrementAmount];
        System.arraycopy(theArray, 0, theNewArray, 0, theArray.length);
        return theNewArray;
    }

    protected String[] growArray(String[] theArray) {
        String[] theNewArray = new String[theArray.length + m_IncrementAmount];
        System.arraycopy(theArray, 0, theNewArray, 0, theArray.length);
        return theNewArray;
    }

    @Override
    public int getMaxBufferSize() {
        return this.m_MaxBufferSize;
    }

    @Override
    public boolean hasContinuationBuffer() {
        return (m_ContinuationIndex > 0);
    }

    @Override
    public int getContinuationIndex() {
        return this.m_ContinuationIndex;
    }

    @Override
    public void setContinuationIndex(int theIndex) {
        this.m_ContinuationIndex = theIndex;
    }

    @Override
    public boolean isDirty() {
        return this.m_IsDirty;
    }

    @Override
    public void setDirty(boolean isDirty) {
        this.m_IsDirty = isDirty;
    }
}
