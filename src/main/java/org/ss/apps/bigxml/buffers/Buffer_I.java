package org.ss.apps.bigxml.buffers;

/**
 * A buffer to store the XML tag data and hashes in a paged, prefixed and randomly accessible format.
 *
 * @author Shyam Sivaraman (shyamsivaraman@amberroad.com) on 05-07-2016.
 */

public interface Buffer_I<T> {

    public boolean append(T theBufferElement);

    public boolean append(T[] theBufferChunk);

    public Buffer_I<T> merge(Buffer_I<T> theBufferToMerge);

    public void pack();

    public void setPrefix(int thePrefix);

    public int getMaxBufferSize();

    public int getActualBufferSize();

    public int[] getBufferPrefixes();

    public boolean hasContinuationBuffer();

    public int getContinuationIndex();

    public void setContinuationIndex(int theIndex);

    /*public void setHasContinuationBuffer(boolean hasContinuation);*/

    public boolean isDirty();

    public void setDirty(boolean isDirty);

    public T get(int theIndex);

}
