package org.ss.apps.bigxml.reader.stax;

import org.ss.apps.bigxml.XMLNode_I;
import org.ss.apps.bigxml.XMLReader_I;

import javax.xml.stream.StreamFilter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Stack;

/**
 *
 * Stream StAX Reader wrapper with single node read-ahead feature to determine if the current cursor location of the underlying
 * StAX parser is a non-Text segment or is a textual segment. The XMLEventReader was not used here as there is no apparent need
 * to store the past events. Also, stream reader may have the minimum memory footprint while parsing the big XML document.
 * The burden to store and reference bygone elements is set as a responsibility of whichever class uses this class, which in case
 * of Big XML integration is the BigEntityContainer. BigEntityContainer may have the responsibility to store, page, load and unload
 * any data parsed by this parser.
 *
 * Date: 29-Mar-16
 *
 * TODO: Add performance logs/ annotations {File Name, File size, Segment count, Time consumed}
 * TODO: Use XMLSegment pool instead of constructing new objects every time. Also provide a copy function on the XMLSegment class
 * TODO: Set backward compatibility w.r.t namespace awareness and validation defaults to
 * how the behaviour was with DOM parser
 * TODO: Additional configurations required for setting Filtered readers, namespace aware, entity references etc.
 *
 * TODO: Fix needed to treat 'NIL' attributes as "" (these may be sent as blank tags to indicate blank data, rather than not sending the tag)
 *
 */

public class StAXReader implements StreamFilter, XMLReader_I {

    private XMLStreamReader m_Reader = null;
    private FileInputStream m_FileStream = null;
    private SegmentBuilder m_SegmentBuilder = null;
    private Stack<String> m_Path = new Stack<String>();

    /**
     * Creates and configures the StAX parser, using the default settings. The filtered reader is
     * set to ignore all events except element starts and text nodes.
     */

    public StAXReader(String theXMLFileName) throws FileNotFoundException, XMLStreamException {
        XMLInputFactory theInputFactory = XMLInputFactory.newInstance();

        StAXConfigurationBuilder theBuilder = new StAXConfigurationBuilder();
        theBuilder.setDefault();
        theBuilder.configure(theInputFactory);

        try {
            createReader(theInputFactory, theXMLFileName);
        } catch(FileNotFoundException theFFEException) {
            if(m_Reader != null)
                closeReader();
            throw theFFEException;
        } catch(XMLStreamException theSEException) {
            if(m_Reader != null)
                closeReader();
            throw theSEException;
        }

        m_SegmentBuilder = new SegmentBuilder();
        m_Path.push("");
    }

    @Override
    public void closeReader() {
        if(m_Reader != null) {
            try {
                m_Reader.close();
            } catch(Exception e) { }
        }

        if(m_FileStream != null) {
            try {
                m_FileStream.close();
            } catch(Exception e) { }
        }
    }

    private void createReader(XMLInputFactory theFactory, String theXMLFileName) throws FileNotFoundException, XMLStreamException {
        m_FileStream = new FileInputStream(theXMLFileName);
        m_Reader = theFactory.createFilteredReader(theFactory.createXMLStreamReader(m_FileStream), this);
    }

    @Override
    public boolean hasNext() throws XMLStreamException {
        return m_SegmentBuilder.hasNextFrame();
    }

    public boolean isCursorWithin(String theTagName) {
        return (m_SegmentBuilder.getCurrentEventFrame().getStackSnapshot().contains(theTagName));
    }

    @Override
    public XMLNode_I nextNode() throws XMLStreamException {
        return m_SegmentBuilder.nextSegment();
    }

    @Override
    public boolean accept(XMLStreamReader theReader) {
        return (theReader.isStartElement() || theReader.isEndElement() ||
                (theReader.isCharacters() && !theReader.isWhiteSpace()));
    }

    /**
     * Inner class which utilizes the StAX reader to return XMSegment nodes for each event. The XMLSegment
     * node contains additional information about the node like node-type, attributes, text-value,
     * parent-node, and its stack frame.
     * In order to determine whether a node is a text-node or not, a read-ahead is done, and so a window of
     * two stack frames is always kept.
     */

    private class SegmentBuilder {
        private StAXXMLNode[] m_Frames = new StAXXMLNode[2];

        private final int ON_START_DOCUMENT = 0;
        private int m_State = ON_START_DOCUMENT;

        public XMLNode_I nextSegment() throws XMLStreamException {
            if(m_State == ON_START_DOCUMENT) {
                m_Frames[0] = createCurrentEventFrame();
                m_Frames[1] = getNextEventFrame();
                m_State = 1;
            } else {
                m_Frames[0] = m_Frames[1];
                m_Frames[1] = getNextEventFrame();
            }

            return m_Frames[0];
        }

        public StAXXMLNode getCurrentEventFrame() {
            return m_Frames[0];
        }

        private StAXXMLNode createCurrentEventFrame() {
            StAXXMLNode theEventFrame;

            theEventFrame = new StAXXMLNode(m_Reader.getName().toString());
            theEventFrame.setNodeType(XMLNode_I.NodeType.NODE_TYPE_SEGMENT);
            theEventFrame.setNodeParent(m_Path.peek());
            theEventFrame.setLevel(m_Path.size()-1);
            m_Path.push(m_Reader.getName().toString());
            theEventFrame.setStackSnapshot(getStackSnapshot(m_Path));
            return theEventFrame;
        }

        private StAXXMLNode getNextEventFrame() throws XMLStreamException {
            StAXXMLNode theEventFrame = null;

            while(m_Reader.hasNext()) {
                m_Reader.next();

                if(m_Reader.isStartElement()) {
                    theEventFrame = createCurrentEventFrame();
                    return theEventFrame;
                } else if(m_Reader.hasText()) {
                    m_Frames[0].setNodeType(XMLNode_I.NodeType.NODE_TYPE_DATA);
                    m_Frames[0].setNodeValue(m_Reader.getText());
                    continue;
                } else if(m_Reader.isEndElement()) {
                    if(m_Path.pop().equals(m_Reader.getName().toString())) {
                        continue;
                    }
                }
            }

            return theEventFrame;
        }

        public boolean hasNextFrame() throws XMLStreamException {
            if(m_State == ON_START_DOCUMENT)
                return m_Reader.hasNext();
            else
                return (m_Frames[1] != null);
        }

        private String getStackSnapshot(Stack<String> theStack) {
            Iterator<String> iter = theStack.iterator();
            StringBuffer theBuffer = new StringBuffer("");
            if(iter.hasNext())
                theBuffer.append(iter.next());

            while(iter.hasNext()) {
                theBuffer.append("/" + iter.next());
            }

            return theBuffer.toString();
        }
    }
}
