package org.ss.apps.bigxml.reader;

import org.ss.apps.bigxml.XMLNode_I;
import org.ss.apps.bigxml.XMLReader_I;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Shyam Sivaraman on 04-07-2016.
 */

public class IDGeneratingXMLReader implements XMLReader_I {

    private XMLReader_I m_Reader;
    private boolean isNotFirst = false;
    private ArrayList<BigDecimal> m_MaxIDAtLevel = new ArrayList<BigDecimal>();

    public IDGeneratingXMLReader(XMLReader_I aReader) {
        this.m_Reader = aReader;
    }

    @Override
    public void closeReader() {
        this.m_Reader.closeReader();
    }

    @Override
    public boolean hasNext() throws XMLStreamException {
        return this.m_Reader.hasNext();
    }

    @Override
    public XMLNode_I nextNode() throws XMLStreamException {
        XMLNode_I aSegment = null;
        BigDecimal theElementId = new BigDecimal(0.0);

        if(isNotFirst) {
            aSegment = m_Reader.nextNode();
            theElementId = generateElementId(aSegment.getLevel(), m_MaxIDAtLevel.get(aSegment.getLevel()-1)).setScale(10, BigDecimal.ROUND_DOWN);
            aSegment.setID(theElementId);
        } else {
            aSegment = m_Reader.nextNode();
            theElementId = generateElementId(aSegment.getLevel(), new BigDecimal(0.0)).setScale(10, BigDecimal.ROUND_DOWN);
            aSegment.setID(theElementId);
            isNotFirst = true;
        }

        return aSegment;
    }

    private BigDecimal generateElementId(int aSegmentLevel, BigDecimal aParentId) {
        BigDecimal theNumericPart = aParentId.multiply(new BigDecimal((10.0)));
        BigDecimal theNewId = new BigDecimal(0.0);

        if(m_MaxIDAtLevel.size() < aSegmentLevel+1) {
            m_MaxIDAtLevel.add(theNumericPart.add(new BigDecimal(0.0)));
        } else if(m_MaxIDAtLevel.get(aSegmentLevel).floatValue() < theNumericPart.floatValue()) {
            m_MaxIDAtLevel.set(aSegmentLevel, (theNumericPart.add(new BigDecimal(0.0))));
        }

        theNewId = m_MaxIDAtLevel.get(aSegmentLevel).add(new BigDecimal(0.1).setScale(10, BigDecimal.ROUND_DOWN));
        m_MaxIDAtLevel.set(aSegmentLevel, theNewId);

        //System.out.println(m_MaxIDAtLevel);

        return theNewId;
    }

    public static void main(String args[]) {
        IDGeneratingXMLReader reader = new IDGeneratingXMLReader(null);
        System.out.println(reader.generateElementId(0, new BigDecimal(0.0)).setScale(10, BigDecimal.ROUND_DOWN));
        /*System.out.println(disk.generateElementId(1, 0.1f));
        System.out.println(disk.generateElementId(2, 1.1f));
        System.out.println(disk.generateElementId(3, 11.1f));
        System.out.println(disk.generateElementId(3, 11.1f));

        System.out.println(disk.generateElementId(2, 1.1f));
        System.out.println(disk.generateElementId(3, 11.2f));
        System.out.println(disk.generateElementId(3, 11.2f));

        System.out.println(disk.generateElementId(1, 0.1f));
        System.out.println(disk.generateElementId(1, 0.1f));
        System.out.println(disk.generateElementId(1, 0.1f));
        System.out.println(disk.generateElementId(2, 1.4f));
        System.out.println(disk.generateElementId(3, 14.1f));*/
    }
}
