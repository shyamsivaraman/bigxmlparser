package org.ss.apps.bigxml.reader.stax;

import org.ss.apps.bigxml.XMLNode_A;

public class StAXXMLNode extends XMLNode_A {

    private String m_StackSnapshot;

    public StAXXMLNode(String theName) {
        super(theName);
    }

    public void setStackSnapshot(String theSnapshot) {
        this.m_StackSnapshot = theSnapshot;
    }

    public String getStackSnapshot() {
        return this.m_StackSnapshot;
    }

}
