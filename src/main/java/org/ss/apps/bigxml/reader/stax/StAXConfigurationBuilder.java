package org.ss.apps.bigxml.reader.stax;

import javax.xml.stream.XMLInputFactory;

public class StAXConfigurationBuilder {

    private String[] m_Configs = {
            XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES,
            XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES,
            XMLInputFactory.IS_NAMESPACE_AWARE,
            XMLInputFactory.IS_COALESCING,
            XMLInputFactory.IS_VALIDATING
    };

    private boolean[] m_ConfigSelection = {false, false, false, false, false};

    public StAXConfigurationBuilder setReplaceEntityReferences() {
        m_ConfigSelection[0] = true;
        return this;
    }

    public StAXConfigurationBuilder setExternalEntitySupport() {
        m_ConfigSelection[1] = true;
        return this;
    }

    public StAXConfigurationBuilder setNamespaceAware() {
        m_ConfigSelection[2] = true;
        return this;
    }

    public StAXConfigurationBuilder setCoalescing() {
        m_ConfigSelection[3] = true;
        return this;
    }

    public StAXConfigurationBuilder setValidating() {
        m_ConfigSelection[4] = true;
        return this;
    }

    public StAXConfigurationBuilder setDefault(){
        m_ConfigSelection[0] = true;
        m_ConfigSelection[1] = true;
        m_ConfigSelection[2] = true;
        m_ConfigSelection[3] = true;
        m_ConfigSelection[4] = false;
        return this;
    }

    public XMLInputFactory configure(XMLInputFactory theFactory) {
        for(int i=0; i<m_ConfigSelection.length; i++)
            theFactory.setProperty(m_Configs[i], m_ConfigSelection[i]);

        return theFactory;
    }

    public String toString() {
        StringBuffer theOut = new StringBuffer();
        for(int i=0; i<m_Configs.length; i++) {
            if(m_ConfigSelection[i])
                theOut.append(m_Configs[i] + " = " + m_ConfigSelection[i] + "\n");
        }

        return theOut.toString();
    }

}
