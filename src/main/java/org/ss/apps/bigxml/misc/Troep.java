package org.ss.apps.bigxml.misc;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;

public class Troep {
    public static void main(String... args) {
        double a = .66 * .453 * .557;
        int number = 200_000_987;
        double prod = a * number;
        double sum = 0;
        float f = 11.1f + 0.1f;
        for (int i = 1; i <= number; i++) sum += a;
        BigDecimal biga = getBigDecimal();
        BigDecimal bignumber = new BigDecimal(new BigInteger("200000987"));
        BigDecimal bigprod = biga.multiply(bignumber);
        BigDecimal bigsum = new BigDecimal(0);
        for (int i = 1; i <= number; i++) bigsum = bigsum.add(biga);

        DecimalFormat df = new DecimalFormat("#,###.####");

        p("a          = " + df.format(a));
        p("prod       = " + df.format(prod));
        p("sum        = " + df.format(sum));
        p("prod - sum = " + df.format(prod - sum));
        p("f          = " + df.format(f));
        p("% error = " + String.format("%15.9f %% %n", 100 * (prod - sum) / prod));
        p("******* BigDecimals **************************");
        p("biga     = " + biga);
        p("bignumber= " + df.format(bignumber));
        p("bigprod  = " + df.format(bigprod));
        p("bigsum   = " + df.format(bigsum));
        p("bigdif   = " + df.format(bigprod.subtract(bigsum)));
    }

    private static void p(String s) {
        System.out.println(s);
    }

    private static BigDecimal getBigDecimal() {
        BigDecimal b66 = new BigDecimal(new BigInteger("66"), 2);
        BigDecimal b453 = new BigDecimal(new BigInteger("453"), 3);
        BigDecimal b557 = new BigDecimal(new BigInteger("557"), 3);
        return b66
                .multiply(b453)
                .multiply(b557)
                ;
    }
}