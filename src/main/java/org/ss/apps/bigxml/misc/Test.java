package org.ss.apps.bigxml.misc;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by m2479 on 05-07-2016.
 */
public class Test {

    public static void main(String[] args) {
        /*float f = 75 * 0.01f;
        System.out.println(f);

        float f1 = ((2*8) * (75 * 0.01f));
        System.out.println(f1);

        int a[] = {1, 2, 3};
        printArray(a);
        System.out.println();
        a = growArray(a, 3);
        printArray(a);*/

        testBinarySearch(301);
    }

    private static int[] growArray(int[] aBufferArray, int m_IncrementAmount) {
        int [] theNewArray = new int[aBufferArray.length + m_IncrementAmount];
        System.arraycopy(aBufferArray, 0, theNewArray, 0, aBufferArray.length);
        return theNewArray;
    }

    private static void printArray(int[] theArray) {
        for(int i=0; i<theArray.length; i++) {
            System.out.print(theArray[i]);
        }
    }

    private static void testBinarySearch(int toSearch) {
        Integer[] theArray = new Integer[20000000];

        long theStartMillis = System.currentTimeMillis();
        for(int i=1; i<2000000001; i++) {
            theArray[i] = i;
        }
        System.out.println("Time to fill the array: " + (System.currentTimeMillis() - theStartMillis));

        theStartMillis = System.currentTimeMillis();
        int theIndex = Collections.binarySearch(Arrays.asList(), toSearch, null);
        System.out.println("Time to find the element 301 = " + toSearch + ": " + (System.currentTimeMillis() - theStartMillis));
    }
}
