package org.ss.apps.bigxml.diskcache;

import org.ss.apps.bigxml.XMLNode_I;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by Shyam Sivaraman on 04-07-2016.
 */

public class XMLDiskPersistence {

    private int m_WriterPointer = 0;
    private int BUFFER_MAX_LENGTH = 1024;
    private BigDecimal[][] m_WriteBuffer = new BigDecimal[10][];
    private int[] m_BufferIndex = new int[10];

    public void persist(BigDecimal theElementId, XMLNode_I theSegment) throws IOException {
        //Get level
        //Check if directory exists for the level, else create it
        File theTempDir = new File("D:/Temp/XMlDump" + File.separator + theSegment.getLevel());
        if(!theTempDir.exists()) {
            theTempDir.mkdirs();
        }

        //Check for file (BigDecimal.round(theElementId) file - For 11.1, 11.2, .... 11.n the file is 11
        File theNodeFile = new File(theTempDir.getCanonicalFile() + File.separator + theElementId.intValue());
        System.out.println(">>> " + theNodeFile.toString());
        if(!theNodeFile.exists()) {
            theNodeFile.createNewFile();
        }

        //File header starts with magic number for index, then load the segment file and append to it
        //Append this elementID to it
        /*if(m_WriterPointer < BUFFER_MAX_LENGTH) {
            m_WriteBuffer[m_WriterPointer++] = theElementId.round(new MathContext(10, RoundingMode.DOWN));
        } else {
            RandomAccessFile theFile = new RandomAccessFile(theNodeFile, "rwd");
            theFile.seek(theNodeFile.length());
            theFile.write();
            theFile.close();
        }*/
    }
}
