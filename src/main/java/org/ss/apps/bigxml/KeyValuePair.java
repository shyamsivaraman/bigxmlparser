package org.ss.apps.bigxml;

/**
 * @author Shyam Sivaraman on 07-07-2016.
 */

public class KeyValuePair {

    public KeyValuePair(int theKey, String theValue) {
        this.m_Key = theKey;
        this.m_Value = theValue;
    }

    public int m_Key;
    public String m_Value;

}
