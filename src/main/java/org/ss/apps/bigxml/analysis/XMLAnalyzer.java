package org.ss.apps.bigxml.analysis;

import org.ss.apps.bigxml.XMLReader_I;
import org.ss.apps.bigxml.reader.stax.StAXReader;

import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;

/**
 * - Hash code collision analysis
 * - Levels analysis
 * - Max child nodes per node analysis
 * - Total number of nodes analysis
 *
 * Estimated nodes in 1 Million BOM: 16000038 nodes (Analysis time: 5 minutes on Quad Core,
 *   2.50 GHz, 4 GB RAM, Win 7 Professional 32-bit, 128M Heap)
 *
 * Created by m2479 on 07-07-2016.
 */

public class XMLAnalyzer {

    private XMLReader_I m_Reader;

    public XMLAnalyzer(XMLReader_I aReader) {
        this.m_Reader = aReader;
    }

    public void run() throws XMLStreamException {
        int i=0;

        while(m_Reader.hasNext()) {
            m_Reader.nextNode();
            i++;
        }

        System.out.println(">> Total nodes found: " + i);
    }

    public static void main(String[] args) throws FileNotFoundException, XMLStreamException {
        StAXReader aStaxReader = new StAXReader("C:\\Users\\m2479\\IdeaProjects\\BigXML\\files\\BOM_1M_1.xml");
        XMLAnalyzer xa = new XMLAnalyzer(aStaxReader);
        xa.run();
    }
}
