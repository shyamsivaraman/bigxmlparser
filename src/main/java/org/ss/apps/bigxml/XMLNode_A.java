package org.ss.apps.bigxml;

import java.math.BigDecimal;

/**
 * Created by Shyam Sivaraman on 05-07-2016.
 */

public abstract class XMLNode_A implements XMLNode_I {

    private String m_TagName;
    private String m_Value;
    private NodeType m_NodeType;
    private String m_ParentTag;
    private int m_Level;
    private BigDecimal m_ID;

    public XMLNode_A(String theName) {
        this.m_TagName = theName;
    }

    @Override
    public void setNodeType(NodeType theNodeType) {
        this.m_NodeType = theNodeType;
    }

    @Override
    public void setNodeName(String theName) {
        this.m_TagName = theName;
    }

    @Override
    public void setNodeValue(String theValue) {
        this.m_Value = theValue;
    }

    @Override
    public NodeType getNodeType() {
        return this.m_NodeType;
    }

    @Override
    public String getNodeName() {
        return this.m_TagName;
    }

    @Override
    public String getNodeValue() {
        return this.m_Value;
    }

    @Override
    public void setNodeParent(String theTagName) {
        this.m_ParentTag = theTagName;
    }

    @Override
    public String getNodeParent() {
        return this.m_ParentTag;
    }

    @Override
    public void setLevel(int aLevel) { this.m_Level = aLevel; }

    @Override
    public int getLevel() { return this.m_Level; }

    @Override
    public void setID(BigDecimal anId) { this.m_ID = anId; }

    @Override
    public BigDecimal getID() { return this.m_ID; }

    @Override
    public String toString() {
        return "[Parent: " + m_ParentTag + "] <" + m_TagName + ">" + ((m_NodeType == NodeType.NODE_TYPE_DATA) ? " " + m_Value + " " : "");
    }
}
