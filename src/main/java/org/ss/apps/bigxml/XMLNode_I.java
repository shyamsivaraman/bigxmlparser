package org.ss.apps.bigxml;

import java.math.BigDecimal;

/**
 * Created by Shyam Sivaraman on 05-07-2016.
 */
public interface XMLNode_I {

    void setNodeType(NodeType theNodeType);

    void setNodeName(String theName);

    void setNodeValue(String theValue);

    NodeType getNodeType();

    String getNodeName();

    String getNodeValue();

    void setNodeParent(String theTagName);

    String getNodeParent();

    String toString();

    void setLevel(int aLevel);

    int getLevel();

    void setID(BigDecimal anId);

    BigDecimal getID();

    public enum NodeType {NODE_TYPE_SEGMENT, NODE_TYPE_DATA}

}
