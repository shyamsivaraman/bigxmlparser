package org.ss.apps.bigxml;

import javax.xml.stream.XMLStreamException;

/**
 * Created by m2479 on 04-07-2016.
 */
public interface XMLReader_I {

    void closeReader();

    boolean hasNext() throws XMLStreamException;

    XMLNode_I nextNode() throws XMLStreamException;
}
